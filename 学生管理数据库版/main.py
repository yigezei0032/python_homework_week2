import pymysql
import dbmanuel, mystu
from stuGui import *


def main():
    # 首先确认本机数据库内是否有用于测试的学生信息数据库，没有就创建一个
    s = input('请输入mysql的用户名：')
    m = input('请输入该用户的密码：')
    dbmanuel.checkdb('localhost', s, m, 'mystunom')
    stu = mystu.myStu('localhost', s, m, 'mystunom')
    while True:
        myGui()
        k = inputMode()
        if k == 1:
            print('(ID,NAME,SEX,AGE)')
            stu.findAll()
            input('按回车键继续:')
        elif k == 2:
            try:
                name = input('请输入要添加学院的姓名:')
                sex = input('请输入该学员的性别[M/F]:')
                age = int(input('请输入该学员的年龄:'))
                stu.insert(name, sex, age)
            except Exception as e:
                print(e)
            input('按回车键继续:')
        elif k == 3:
            try:
                id = int(input('请输入需要删除学生的ID号:'))
                stu.delinfo(id)
            except Exception as e:
                print(e)
            input('按回车键继续:')
        elif k == 4:
            del stu
            break
        else:
            pass
    key = input('是否要删除测试用数据库？[y/n]:')
    if key == 'y' or key == 'Y':
        dbmanuel.deltestDB('localhost', s, m, 'mystunom')


if __name__ == '__main__':
    main()
