def myGui():
    print('=' * 12, '学员管理系统', '=' * 14)
    print('{0:1} {1:13} {2:15}'.format(' ', '1.查看学员信息', '2.添加学员信息'))
    print('{0:1} {1:13} {2:15}'.format(' ', '3.删除学员信息', '4.退出系统'))
    print('=' * 37)


def inputMode():
    key = input('请输入对应的操作:')
    if key == '1':
        print('=' * 14, '查看学员信息', '=' * 14)
        # input('按回车键继续:')
    elif key == '2':
        print('=' * 14, '添加学员信息', '=' * 14)
        # input('按回车键继续:')
    elif key == '3':
        print('=' * 14, '删除学员信息', '=' * 14)
        # input('按回车键继续:')
    elif key == '4':
        print('=' * 14, '再见', '=' * 14)
    else:
        key = 0
        print('=' * 14, '无效的输入!', '=' * 14)
    return int(key)


if __name__ == '__main__':
    myGui()
    k = inputMode()
    print(k)
