-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: blogdb
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `abstract` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `uid` int(10) unsigned DEFAULT NULL,
  `pcount` int(10) unsigned DEFAULT '0',
  `flag` tinyint(3) unsigned DEFAULT '0',
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'a','hello','bonjour tout le monde',2,10,0,'2017-12-01 00:00:00'),(2,'b','hello','bonjour tout le monde',2,11,0,'2017-12-02 00:00:00'),(3,'c','hello','bonjour tout le monde',1,1,0,'2017-12-02 00:00:00'),(4,'d','hello','bonjour tout le monde',5,13,0,'2017-12-02 00:00:00'),(5,'e','hello','bonjour tout le monde',4,133,0,'2017-12-02 00:00:00'),(6,'f','hello','bonjour tout le monde',3,153,0,'2027-02-02 00:00:00'),(7,'j','hello','bonjour tout le monde',10,53,0,'2017-05-02 00:00:00'),(8,'h','hello','bonjour tout le monde',11,93,0,'2017-05-15 00:00:00'),(9,'i','hello','bonjour tout le monde',8,83,0,'2017-05-25 00:00:00'),(10,'g','hello','bonjour tout le monde',7,43,0,'2017-06-25 00:00:00'),(11,'k','hello','bonjour tout le monde',2,423,0,'2017-08-25 00:00:00'),(12,'l','hello','bonjour tout le monde',3,323,0,'2017-08-15 00:00:00');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'no1','1@a.com','2000-01-01 00:00:00'),(2,'no2','2@a.com','2001-01-01 00:00:00'),(3,'no3','3@a.com','2002-01-01 00:00:00'),(4,'no4','4@a.com','2003-01-01 00:00:00'),(5,'no5','5@a.com','2004-01-01 00:00:00'),(6,'no6','6@a.com','2005-01-01 00:00:00'),(7,'no7','7@a.com','2006-01-01 00:00:00'),(8,'no8','8@a.com','2007-01-01 00:00:00'),(9,'no9','9@a.com','2008-01-01 00:00:00'),(10,'no10','10@a.com','2009-01-01 00:00:00'),(11,'no11','11@a.com','2010-01-01 00:00:00'),(12,'no12','11@a.com','2011-01-01 00:00:00'),(13,'no13','11@a.com','2012-01-01 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-25 16:39:03
